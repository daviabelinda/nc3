//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene2: SKScene {
    
    let defaults = UserDefaults.standard

    let bambang = SKTAudio.sharedInstance()
    
    var StartButton: SKSpriteNode!
    var PlayerButton: SKLabelNode!
    var BotButton: SKLabelNode!
    var HowToPlayButton: SKLabelNode!
    
    var backgroundMusic: SKAudioNode!
    
    var touchplayer = 0
    var touchbot = 0
    var open = 0
    
    var player: AVAudioPlayer = AVAudioPlayer()
        
    override func didMove(to view: SKView) {

        bambang.resumeBackgroundMusic()
        
        StartButton = (childNode(withName: "startbutton") as! SKSpriteNode)
        PlayerButton = (childNode(withName: "playerbutton") as! SKLabelNode)
        BotButton = (childNode(withName: "botbutton") as! SKLabelNode)
        HowToPlayButton = (childNode(withName: "howtoplaybutton") as! SKLabelNode)
        
        bambang.playBackgroundMusic("bgm nc3.mp3")

    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == StartButton{
                if open == 1{
                    if let view = self.view {
                        // Load the SKScene from 'GameScene.sks'
                        print("Player")
                        if let scene = SKScene(fileNamed: "GameScene3") {
                            // Set the scale mode to scale to fit the window
                            scene.scaleMode = .aspectFill
                            
                            // Present the scene
                            view.presentScene(scene)
                        }
                        
                        view.ignoresSiblingOrder = true
                        
                        view.showsFPS = false
                        view.showsNodeCount = false
                    }
                }
                else if open == 3{
                    if let view = self.view {
                        print("Bot")
                        
                        // Load the SKScene from 'GameScene.sks'
                        if let scene = SKScene(fileNamed: "GameScene3Bot") {
                            // Set the scale mode to scale to fit the window
                            scene.scaleMode = .aspectFill
                            
                            // Present the scene
                            view.presentScene(scene)
                        }
                        
                        view.ignoresSiblingOrder = true
                        
                        view.showsFPS = false
                        view.showsNodeCount = false
                    }
                    
                }
            }
        }
        
        if let node = self.nodes(at: touch.location(in: self)).first as? SKLabelNode{
            if node == PlayerButton{
                if touchplayer == 0 {
                    touchplayer = 1
                    touchbot = 0
                    PlayerButton.fontColor = UIColor.black
                    BotButton.fontColor = UIColor.gray
                    
                    open = 1
                }
                else{
                    touchplayer = 0
                    PlayerButton.fontColor = UIColor.gray
                    open = 0
                }
            }
            else if node == BotButton{
                if touchbot == 0 {
                    touchbot = 1
                    touchplayer = 0
                    BotButton.fontColor = UIColor.black
                    PlayerButton.fontColor = UIColor.gray
                    open = 3
                }
                else{
                    touchbot = 0
                    BotButton.fontColor = UIColor.gray
                    open = 0
                }
            }
            else if BotButton.fontColor != UIColor.black || PlayerButton.fontColor != UIColor.black{
                open = 0
            }
            if node == HowToPlayButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "HowToPlay") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
}
