//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene5Bot: SKScene {
    
    var LeftButton: SKSpriteNode!
    var RightButton: SKSpriteNode!
    var LeftBuilding: SKSpriteNode!
    var RightBuilding: SKSpriteNode!
    
    var CountDown: SKLabelNode!
    var BackButton: SKSpriteNode!
    
    var LeftDirection: CGFloat = 0.0
    var RightDirection: CGFloat = 0.0
    
    var timers = 3
    
    var NilaiRandom = ""
    var random = ["1","2","2","1","3","1"]
    
    var timeinterval = 0.0
    
    var player: AVAudioPlayer = AVAudioPlayer()
    var tap: AVAudioPlayer = AVAudioPlayer()
    let bambang = SKTAudio.sharedInstance()
    
    override func didMove(to view: SKView) {
        
        bambang.pauseBackgroundMusic()
        playsound()
        player.numberOfLoops = -1
        player.play()
        
        self.view?.isUserInteractionEnabled = false
        
        LeftButton = (childNode(withName: "leftbutton") as! SKSpriteNode)
        RightButton = (childNode(withName: "rightbutton") as! SKSpriteNode)
        
        LeftBuilding = (childNode(withName: "leftbuilding") as! SKSpriteNode)
        RightBuilding = (childNode(withName: "rightbuilding") as! SKSpriteNode)
        
        CountDown = (childNode(withName: "countdown") as! SKLabelNode)
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        
        CountDown.text = "3"
        
        LeftButton.color = UIColor.clear
        RightButton.color = UIColor.clear
        
        LeftButton.isHidden = true
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self.timers == 0 {
                timer.invalidate()
                self.CountDown.isHidden = true
                self.view?.isUserInteractionEnabled = true
                
            }
            else{
                self.timers-=1
                self.CountDown.text = "\(self.timers)"
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            self.getrandominterval()
            Timer.scheduledTimer(withTimeInterval: self.timeinterval, repeats: true) { timer in
                self.getrandomnumber()
                if self.NilaiRandom == "1" || self.NilaiRandom == "2" || self.NilaiRandom == "3"{
                    self.LeftDirection = -1
                    self.getrandomnumber()
                }
            }
            
        }
        
    }
    
    func playsound(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "game2", ofType: "mp3")
            
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    func playtap(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "taptap", ofType: "mp3")
            
            try tap = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        playtap()
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == LeftButton{
                LeftDirection = -1
                tap.play()
            }
            else if node == RightButton{
                RightDirection = -1
                tap.play()
            }
        }
        
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == BackButton{
                player.stop()
                bambang.pauseBackgroundMusic()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene3Bot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        LeftDirection = 0
        RightDirection = 0
        tap.stop()
    }
    
    override func update(_ currentTime: TimeInterval) {
        let Leftnewposition = LeftBuilding.position.y + (LeftDirection * 20)
        LeftBuilding.position.y = Leftnewposition
        LeftDirection = 0
        
        let Rightnewposition = RightBuilding.position.y + (RightDirection * 20)
        RightBuilding.position.y = Rightnewposition
        RightDirection = 0
        
        if Leftnewposition <= -600 {
            if let view = self.view {
                player.stop()
                // Load the SKScene from 'GameScene.sks'
                if let scene = SKScene(fileNamed: "WinningScene2Bot") {
                    // Set the scale mode to scale to fit the window
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    view.presentScene(scene)
                }
                
                view.ignoresSiblingOrder = true
                
                view.showsFPS = false
                view.showsNodeCount = false
            }
            
        }
        else if Rightnewposition <= -600{
            player.stop()
            if let view = self.view {
                // Load the SKScene from 'GameScene.sks'
                if let scene = SKScene(fileNamed: "WinningSceneBot") {
                    // Set the scale mode to scale to fit the window
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    view.presentScene(scene)
                }
                
                view.ignoresSiblingOrder = true
                
                view.showsFPS = false
                view.showsNodeCount = false
            }
        }
        
    }
    
    func getrandomnumber(){
        NilaiRandom = "\(random.randomElement()!)"
    }
    
    func getrandominterval(){
        
        let nilairandominterval = "\(random.randomElement()!)"
        
        if nilairandominterval == "1"{
            timeinterval = 0.1
        }
        else if nilairandominterval == "2"{
            timeinterval = 0.15
        }
        else if nilairandominterval == "3"{
            timeinterval = 0.17
        }
    }
    
}
