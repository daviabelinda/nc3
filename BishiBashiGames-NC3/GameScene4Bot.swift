//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene4Bot: SKScene {
    
    var AnggukAction: SKAction!
    var GelengAction: SKAction!
    var CewekAnggukAction: SKAction!
    var CewekGelengAction: SKAction!
    
    var RightMan: SKSpriteNode!
    var LeftGirl: SKSpriteNode!
    
    var Tv: SKSpriteNode!
    var Plate: SKSpriteNode!
    
    var LeftScore: SKLabelNode!
    var RightScore: SKLabelNode!
    
    var BackButton: SKSpriteNode!
    var CountDown: SKLabelNode!
    
    var flag = ""
    var FlagMatch = 3
    
    var TvImage = ["kue1", "kue2", "kue3", "kue4", "kue5","kue2", "kue1", "kue2", "kue1"]
    var PlateImage = ["kue1", "kue2", "kue3", "kue4", "kue5", "kue2", "kue1", "kue2", "kue1"]
    
    var GirlScore = 0
    var ManScore = 0
    
    var timers = 3
    
    var random = ["1","2","3","2","2","3"]
    var nilairandom = ""
    var intervaltime = 0.0
    
    var player: AVAudioPlayer = AVAudioPlayer()
    var tap: AVAudioPlayer = AVAudioPlayer()
    let bambang = SKTAudio.sharedInstance()
    
    private var initialTouch: CGPoint = CGPoint.zero
    
    override func didMove(to view: SKView) {
        
        bambang.pauseBackgroundMusic()
        
        
        self.view?.isUserInteractionEnabled = false
        
        addSwipeGesture()
        setupNgangguk()
        setupGeleng()
        setupGelengCewek()
        setupNganggukCewek()
        
        playsound()
        player.numberOfLoops = -1
        player.play()
        
        
        RightMan = (childNode(withName: "rightman") as! SKSpriteNode)
        LeftGirl = (childNode(withName: "leftgirl") as! SKSpriteNode)
        
        Tv = (childNode(withName: "tv") as! SKSpriteNode)
        Plate = (childNode(withName: "plate") as! SKSpriteNode)
        
        LeftScore = (childNode(withName: "leftscore") as! SKLabelNode)
        RightScore = (childNode(withName: "rightscore") as! SKLabelNode)
        
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        CountDown = (childNode(withName: "countdown") as! SKLabelNode)
        
        randomimage()
        
        CountDown.text = "3"
        
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self.timers == 0 {
                timer.invalidate()
                self.CountDown.isHidden = true
                self.view?.isUserInteractionEnabled = true
                
            }
            else{
                self.timers-=1
                self.CountDown.text = "\(self.timers)"
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.getrandom()
            Timer.scheduledTimer(withTimeInterval: self.intervaltime, repeats: true) { timer in
                self.getrandom()
                if self.GirlScore <= 4{
                    if self.FlagMatch == 1{
                        self.LeftGirl.run(self.CewekAnggukAction, withKey:"anggukAnimation")
                        self.GirlScore+=1
                        self.LeftScore.text = "\(self.GirlScore)"
                        self.FlagMatch = 3
                        self.randomimage()
                    }
                    else if self.FlagMatch == 0{
                        self.LeftGirl.run(self.CewekGelengAction, withKey:"anggukAnimation")
                        self.randomimage()
                    }
                }
                else{
                    self.player.stop()
                    if let view = self.view {
                        // Load the SKScene from 'GameScene.sks'
                        if let scene = SKScene(fileNamed: "WinningScene2Bot") {
                            // Set the scale mode to scale to fit the window
                            scene.scaleMode = .aspectFill
                            
                            // Present the scene
                            view.presentScene(scene)
                        }
                        
                        view.ignoresSiblingOrder = true
                        
                        view.showsFPS = false
                        view.showsNodeCount = false
                    }
                }
                
            }
        }
    }
    
    func playsound(){
           do{
               let audioPlayer = Bundle.main.path(forResource: "game3", ofType: "mp3")
               
               try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
               
           }
           catch{
               
           }
       }
       
       func playtap(){
           do{
               let audioPlayer = Bundle.main.path(forResource: "swipe", ofType: "mp3")
               
               try tap = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
               
           }
           catch{
               
           }
       }
    
    func getrandom(){
        nilairandom = "\(random.randomElement()!)"
        
        if nilairandom == "1" {
            intervaltime = 0.5
        }
        else if nilairandom == "2" {
            intervaltime = 1.5
        }
        else if nilairandom == "3" {
            intervaltime = 2.5
        }
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    func randomimage(){
        let TvRandom = TvImage.randomElement()
        let PlateRandom = PlateImage.randomElement()
        
        Tv.texture = SKTexture(imageNamed: "\(TvRandom!)")
        Plate.texture = SKTexture(imageNamed: "\(PlateRandom!)")
        
        if TvRandom == PlateRandom {
            FlagMatch = 1
        }
        else{
            FlagMatch = 0
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first as UITouch?{
            initialTouch = touch.location(in: self.scene!.view)
            if(initialTouch.x > self.view!.bounds.size.width/2){
                flag = "Right"
            }
            //            else{
            //                flag = "Left"
            //
            //            }
            
            if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
                if node == BackButton{
                    player.stop()
                    if let view = self.view {
                        // Load the SKScene from 'GameScene.sks'
                        if let scene = SKScene(fileNamed: "GameScene3Bot") {
                            // Set the scale mode to scale to fit the window
                            scene.scaleMode = .aspectFill
                            
                            // Present the scene
                            view.presentScene(scene)
                        }
                        
                        view.ignoresSiblingOrder = true
                        
                        view.showsFPS = false
                        view.showsNodeCount = false
                    }
                }
            }
        }
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    func addSwipeGesture(){
        let gesturesdirection: [UISwipeGestureRecognizer.Direction] = [.up,.right,.down,.left]
        
        for gesture in gesturesdirection{
            let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleswipe))
            gestureRecognizer.direction = gesture
            self.view?.addGestureRecognizer(gestureRecognizer)
        }
    }
    
    @objc func handleswipe(gesture: UISwipeGestureRecognizer){
        if let gesture = gesture as? UISwipeGestureRecognizer{
            if ManScore >= 4 {
                player.stop()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "WinningSceneBot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
            else if GirlScore >= 4 {
                player.stop()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "WinningScene2Bot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
            else{
                switch gesture.direction {
                    
                    
                //ANGGUK ATAS
                case .up:
                    if flag == "Right" {
                        playtap()
                        tap.play()
                        RightMan.run(AnggukAction, withKey:"anggukAnimation")
                        randomimage()
                        if FlagMatch == 1{
                            ManScore+=1
                            RightScore.text = "\(ManScore)"
                            FlagMatch = 3
                        }
                        //                    else if FlagMatch == 0{
                        //                        if ManScore != 0{
                        //                            ManScore-=1
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //                        else if ManScore == 0{
                        //                            ManScore=0
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //
                        //                    }
                        
                    }
                    else if flag == "Left"{
                        playtap()
                        tap.play()
                        LeftGirl.run(CewekAnggukAction, withKey:"anggukAnimation")
                        randomimage()
                        if FlagMatch == 1{
                            GirlScore+=1
                            LeftScore.text = "\(GirlScore)"
                            FlagMatch = 3
                            
                        }
                        //                    else if FlagMatch == 0{
                        //                        if GirlScore != 0{
                        //                            GirlScore-=1
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                        else if GirlScore == 0{
                        //                            GirlScore=0
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //
                        //                    }
                        
                    }
                    
                    
                    
                    
                //ANGGUK BAWAH
                case .down:
                    if flag == "Right" {
                        playtap()
                        tap.play()
                        RightMan.run(AnggukAction, withKey:"anggukAnimation")
                        if FlagMatch == 1{
                            ManScore+=1
                            RightScore.text = "\(ManScore)"
                            FlagMatch = 3
                            
                        }
                        //                    else if FlagMatch == 0{
                        //                        if ManScore != 0{
                        //                            ManScore-=1
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //                        else if ManScore == 0{
                        //                            ManScore=0
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //
                        //                    }
                        randomimage()
                        
                    }
                    else if flag == "Left"{
                        playtap()
                        tap.play()
                        LeftGirl.run(CewekAnggukAction, withKey:"anggukAnimation")
                        if FlagMatch == 1{
                            GirlScore+=1
                            LeftScore.text = "\(GirlScore)"
                            FlagMatch = 3
                            
                        }
                        //                    else if FlagMatch == 0{
                        //                        if GirlScore != 0{
                        //                            GirlScore-=1
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                        else if GirlScore == 0{
                        //                            GirlScore=0
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                    }
                        randomimage()
                        
                    }
                    
                    
                    
                //GELENG KANAN
                case .right:
                    if flag == "Right" {
                        playtap()
                        tap.play()
                        RightMan.run(GelengAction, withKey:"anggukAnimation")
                        //                        if FlagMatch == 0{
                        //                            ManScore+=1
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //                    else if FlagMatch == 1{
                        //                        if ManScore != 0{
                        //                            ManScore-=1
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //                        else if ManScore == 0{
                        //                            ManScore=0
                        //                            RightScore.text = "\(ManScore)"
                        //                        }
                        //                    }
                        randomimage()
                        
                    }
                    
                    
                    
                //GELENG KIRI
                case .left:
                    if flag == "Left"{
                        playtap()
                        tap.play()
                        LeftGirl.run(CewekGelengAction, withKey:"anggukAnimation")
                        //                        if FlagMatch == 0{
                        //                            GirlScore+=1
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                    else if FlagMatch == 1{
                        //                        if GirlScore != 0{
                        //                            GirlScore-=1
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                        else if GirlScore == 0{
                        //                            GirlScore=0
                        //                            LeftScore.text = "\(GirlScore)"
                        //                        }
                        //                    }
                        randomimage()
                    }
                    
                default:
                    print("No Gesture")
                }
            }
        }
        
    }
    
    func setupNgangguk(){
        var textures = [SKTexture]()
        for i in 1...2{
            textures.append(SKTexture(imageNamed: "Angguk \(i)"))
        }
        textures.append(SKTexture(imageNamed: "Santuy"))
        AnggukAction = SKAction.repeat(SKAction.animate(with: textures, timePerFrame: 0.1), count: 2)
        tap.stop()
    }
    
    func setupGeleng(){
        var textures = [SKTexture]()
        for i in 1...2{
            textures.append(SKTexture(imageNamed: "Geleng \(i)"))
        }
        textures.append(SKTexture(imageNamed: "Santuy"))
        GelengAction = SKAction.repeat(SKAction.animate(with: textures, timePerFrame: 0.1), count: 2)
        tap.stop()
    }
    
    func setupGelengCewek(){
        var textures = [SKTexture]()
        for i in 1...2{
            textures.append(SKTexture(imageNamed: "CewekGeleng\(i)"))
        }
        textures.append(SKTexture(imageNamed: "CewekSantuy"))
        CewekGelengAction = SKAction.repeat(SKAction.animate(with: textures, timePerFrame: 0.1), count: 2)
        tap.stop()
    }
    
    func setupNganggukCewek(){
        var textures = [SKTexture]()
        for i in 1...2{
            textures.append(SKTexture(imageNamed: "CewekNgangguk\(i)"))
        }
        textures.append(SKTexture(imageNamed: "CewekSantuy"))
        CewekAnggukAction = SKAction.repeat(SKAction.animate(with: textures, timePerFrame: 0.1), count: 2)
        tap.stop()
    }
    
}
