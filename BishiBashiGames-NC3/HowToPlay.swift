//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class HowToPlay: SKScene {
    
    var LeftButton: SKSpriteNode!
    var RightButton: SKSpriteNode!
    var BackButton: SKSpriteNode!
    var Tap: SKSpriteNode!
    var Tap2: SKSpriteNode!
    var TutorialBackground: SKSpriteNode!
    
    private let SKTAudioInstance = SKTAudio.sharedInstance()
        
    var tutor = 1
    
    override func didMove(to view: SKView) {
        LeftButton = (childNode(withName: "leftbutton") as! SKSpriteNode)
        RightButton = (childNode(withName: "rightbutton") as! SKSpriteNode)
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        Tap = (childNode(withName: "tap") as! SKSpriteNode)
        Tap2 = (childNode(withName: "tap2") as! SKSpriteNode)
        TutorialBackground = (childNode(withName: "tutorialimage") as! SKSpriteNode)
        
        LeftButton.isHidden = true
        Tap2.isHidden = true
        
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == BackButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene2") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            }
            else if node == RightButton{
                if tutor == 1{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial1-22")
                    Tap.isHidden = false
                    Tap2.isHidden = true
                    tutor += 1
                }
                else if tutor == 2{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial2-22")
                    LeftButton.isHidden = false
                    RightButton.isHidden = false
                    Tap.isHidden = true
                    Tap2.isHidden = false
                    tutor += 1
                }
                else if tutor == 3{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial3-22")
                    RightButton.isHidden = true
                    Tap.isHidden = true
                    Tap2.isHidden = true
                }
            }
            else if node == LeftButton{
                if tutor == 1{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial1-22")
                    LeftButton.isHidden = true
                    RightButton.isHidden = false
                    Tap.isHidden = false
                    Tap2.isHidden = true
                }
                else if tutor == 2{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial2-22")
                    LeftButton.isHidden = false
                    RightButton.isHidden = false
                    Tap.isHidden = true
                    Tap2.isHidden = false
                    tutor-=1
                }
                else if tutor == 3{
                    TutorialBackground.texture = SKTexture(imageNamed: "tutorial3-22")
                    RightButton.isHidden = false
                    Tap.isHidden = true
                    Tap2.isHidden = true
                    tutor-=1
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
}
