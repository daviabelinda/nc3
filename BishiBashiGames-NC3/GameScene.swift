//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene: SKScene {
    
    var Character: SKSpriteNode!
    var LeftButton: SKSpriteNode!
    var RightButton: SKSpriteNode!
    var LeftCake: SKSpriteNode!
    var RightCake: SKSpriteNode!
    
    var BackButton: SKSpriteNode!
    var CountDown: SKLabelNode!
    
    var CharacterDirection: CGFloat = 0.0
    
    var LeftCakeEaten = 0
    var RightCakeEaten = 0
    
    var flag = 0
    
    var timers = 3
    
    var NilaiRandom = ""
    var random = ["1","2","1","1","1","1", "1", "1", "1", "1", "1", "1", "1"]
    
    let bambang = SKTAudio.sharedInstance()
    
    var player:AVAudioPlayer = AVAudioPlayer()
    var tap:AVAudioPlayer = AVAudioPlayer()


    override func didMove(to view: SKView) {
        
        bambang.pauseBackgroundMusic()
        self.view?.isUserInteractionEnabled = false
        
        Character = (childNode(withName: "character") as! SKSpriteNode)
        LeftButton = (childNode(withName: "leftbutton") as! SKSpriteNode)
        RightButton = (childNode(withName: "rightbutton") as! SKSpriteNode)
        LeftCake = (childNode(withName: "leftcake") as! SKSpriteNode)
        RightCake = (childNode(withName: "rightcake") as! SKSpriteNode)
        
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        CountDown = (childNode(withName: "countdown") as! SKLabelNode)
        RightCake.xScale = RightCake.xScale * -1
        
        LeftButton.color = UIColor.clear
        RightButton.color = UIColor.clear
        
//        LeftButton.isHidden = true
        
        CountDown.text = "3"
        
        playsound()
        player.numberOfLoops = -1
        player.play()
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self.timers == 0 {
                timer.invalidate()
                self.CountDown.isHidden = true
                self.view?.isUserInteractionEnabled = true
            }
            else{
                self.timers-=1
                self.CountDown.text = "\(self.timers)"
            }
        }
    }
    
    func playsound(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "candyland", ofType: "mp3")
            
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    func playtap(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "taptap", ofType: "mp3")
            
            try tap = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == LeftButton{
                CharacterDirection = -1
                playtap()
                tap.play()
            }
            else if node == RightButton{
                CharacterDirection = 1
                playtap()
                tap.play()
            }
        }
        
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == BackButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene3") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            }
        }
        
        if Character.position.x + (CharacterDirection * 100) == 504.47998046875 || Character.position.x + (CharacterDirection * 100) == 500.08001708984375{
            if LeftCakeEaten == 0{
                let texture1 = SKTexture(imageNamed: "cake1-5")
                LeftCake.texture = texture1
                LeftCakeEaten = 1
                flag = 1
            }
            else if LeftCakeEaten == 1{
                let texture1 = SKTexture(imageNamed: "cake1-6")
                LeftCake.texture = texture1
                LeftCakeEaten = 2
                flag = 1
            }
            else if LeftCakeEaten == 2{
                let texture1 = SKTexture(imageNamed: "cake1-7")
                LeftCake.texture = texture1
                LeftCakeEaten = 3
                flag = 1
            }
            else if LeftCakeEaten == 3{
                let texture1 = SKTexture(imageNamed: "cake1-8")
                LeftCake.texture = texture1
                LeftCakeEaten = 4
                flag = 1
            }
            else{
                player.stop()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "WinningScene2") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
        else if Character.position.x + (CharacterDirection * 100) == 1304.47998046875 || Character.position.x + (CharacterDirection * 100) == 1300.080078125{
            if RightCakeEaten == 0{
                let texture2 = SKTexture(imageNamed: "cake2-5")
                RightCake.texture = texture2
                RightCakeEaten = 1
                flag = 1
            }
            else if RightCakeEaten == 1{
                let texture2 = SKTexture(imageNamed: "cake2-6")
                RightCake.texture = texture2
                RightCakeEaten = 2
                flag = 1
            }
            else if RightCakeEaten == 2{
                let texture2 = SKTexture(imageNamed: "cake2-7")
                RightCake.texture = texture2
                RightCakeEaten = 3
                flag = 1
            }
            else if RightCakeEaten == 3{
                let texture1 = SKTexture(imageNamed: "cake2-8")
                RightCake.texture = texture1
                RightCakeEaten = 4
                flag = 1
            }
            else{
                player.stop()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "WinningScene") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        CharacterDirection = 0
        tap.stop()
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
        let newposition = Character.position.x + (CharacterDirection * 100)
        CharacterDirection = 0
        if newposition < 1400 && newposition > 450 && flag == 0{
            Character.position.x = newposition
        }
        else if newposition < 1400 && newposition > 450 && flag == 1{
            Character.position.x = 904.48
            flag = 0
        }
    }
    
    func getrandomnumber(){
        NilaiRandom = "\(random.randomElement()!)"
    }
    
}
