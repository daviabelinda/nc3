//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class WinningScene2: SKScene {
    
    var HomeButton: SKSpriteNode!

    var player:AVAudioPlayer = AVAudioPlayer()
    
    override func didMove(to view: SKView) {
        self.view?.isUserInteractionEnabled = false
        self.HomeButton = (self.childNode(withName: "homebutton") as! SKSpriteNode)
        self.HomeButton.color = UIColor.clear
        
        playsound()
        player.play()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
            self.view?.isUserInteractionEnabled = true
        }
    }
    
    func playsound(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "clapping", ofType: "mp3")
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
        }
        catch{
            
        }
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        player.stop()
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == HomeButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene3") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
}
