//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene3Bot: SKScene {
    
    var CakeButton: SKSpriteNode!
    var TvButton: SKSpriteNode!
    var IslandButton: SKSpriteNode!
    var BackButton: SKSpriteNode!
    
    var player:AVAudioPlayer = AVAudioPlayer()

    let bambang = SKTAudio.sharedInstance()

    override func didMove(to view: SKView) {
        bambang.resumeBackgroundMusic()
        CakeButton = (childNode(withName: "cakebutton") as! SKSpriteNode)
        TvButton = (childNode(withName: "tvbutton") as! SKSpriteNode)
        IslandButton = (childNode(withName: "islandbutton") as! SKSpriteNode)
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == CakeButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameSceneBot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
            else if node == IslandButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene5Bot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill

                        // Present the scene
                        view.presentScene(scene)
                    }

                    view.ignoresSiblingOrder = true

                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
            else if node == TvButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene4Bot") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
            
            
        }
        
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == BackButton{
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene2") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
}
