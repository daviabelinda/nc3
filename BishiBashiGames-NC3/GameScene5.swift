//
//  GameScene.swift
//  BishiBashiGames-NC3
//
//  Created by Stendy Antonio on 10/06/20.
//  Copyright © 2020 Stendy Antonio. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene5: SKScene {
    
    var LeftButton: SKSpriteNode!
    var RightButton: SKSpriteNode!
    var LeftBuilding: SKSpriteNode!
    var RightBuilding: SKSpriteNode!
    
    var CountDown: SKLabelNode!
    var BackButton: SKSpriteNode!
    
    var LeftDirection: CGFloat = 0.0
    var RightDirection: CGFloat = 0.0
    
    let bambang = SKTAudio.sharedInstance()
    
    var timers = 3
    
    var NilaiRandom = ""
    var random = ["1","2","1","1"]
    
    var player: AVAudioPlayer = AVAudioPlayer()
    var tap: AVAudioPlayer = AVAudioPlayer()

    
    override func didMove(to view: SKView) {
        
        bambang.pauseBackgroundMusic()
        
        self.view?.isUserInteractionEnabled = false
        
        LeftButton = (childNode(withName: "leftbutton") as! SKSpriteNode)
        RightButton = (childNode(withName: "rightbutton") as! SKSpriteNode)
        
        LeftBuilding = (childNode(withName: "leftbuilding") as! SKSpriteNode)
        RightBuilding = (childNode(withName: "rightbuilding") as! SKSpriteNode)
        
        CountDown = (childNode(withName: "countdown") as! SKLabelNode)
        BackButton = (childNode(withName: "backbutton") as! SKSpriteNode)
        
        CountDown.text = "3"
        
        LeftButton.color = UIColor.clear
        RightButton.color = UIColor.clear
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self.timers == 0 {
                timer.invalidate()
                self.CountDown.isHidden = true
                self.view?.isUserInteractionEnabled = true
                
            }
            else{
                self.timers-=1
                self.CountDown.text = "\(self.timers)"
            }
        }
        
        playsound()
        player.numberOfLoops = -1
        player.play()
        
    }
    
    func playsound(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "game2", ofType: "mp3")
            
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    func playtap(){
        do{
            let audioPlayer = Bundle.main.path(forResource: "taptap", ofType: "mp3")
            
            try tap = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
            
        }
        catch{
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == LeftButton{
                LeftDirection = -1
                playtap()
                tap.play()
            }
            else if node == RightButton{
                RightDirection = -1
                playtap()
                tap.play()
            }
        }
        
        if let node = self.nodes(at: touch.location(in: self)).first as? SKSpriteNode{
            if node == BackButton{
                player.stop()
                if let view = self.view {
                    // Load the SKScene from 'GameScene.sks'
                    if let scene = SKScene(fileNamed: "GameScene3") {
                        // Set the scale mode to scale to fit the window
                        scene.scaleMode = .aspectFill
                        
                        // Present the scene
                        view.presentScene(scene)
                    }
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        LeftDirection = 0
        RightDirection = 0
        tap.stop()
    }
    
    override func update(_ currentTime: TimeInterval) {
        let Leftnewposition = LeftBuilding.position.y + (LeftDirection * 20)
        LeftBuilding.position.y = Leftnewposition
        LeftDirection = 0
        
        let Rightnewposition = RightBuilding.position.y + (RightDirection * 20)
        RightBuilding.position.y = Rightnewposition
        RightDirection = 0
        
        if Leftnewposition <= -600 {
            player.stop()
            if let view = self.view {
                // Load the SKScene from 'GameScene.sks'
                if let scene = SKScene(fileNamed: "WinningScene2") {
                    // Set the scale mode to scale to fit the window
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    view.presentScene(scene)
                }
                
                view.ignoresSiblingOrder = true
                
                view.showsFPS = false
                view.showsNodeCount = false
            }
            
        }
        else if Rightnewposition <= -600{
            player.stop()
            if let view = self.view {
                // Load the SKScene from 'GameScene.sks'
                if let scene = SKScene(fileNamed: "WinningScene") {
                    // Set the scale mode to scale to fit the window
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    view.presentScene(scene)
                }
                
                view.ignoresSiblingOrder = true
                
                view.showsFPS = false
                view.showsNodeCount = false
            }
        }
        
    }
    
    func getrandomnumber(){
        NilaiRandom = "\(random.randomElement()!)"
    }
    
}
